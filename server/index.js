const express = require("express");
const http = require("http");
const cors = require('cors');
const socketIo = require("socket.io");

const port = process.env.PORT || 3001;

let app = express();

app.use(cors({
  origin: 'http://localhost:3000'
}));

const server = http.createServer(app);

const io = socketIo(server, {cors: true, origins: ['http://localhost:3000']});

io.on("connection", (socket) => {
  console.log(socket.id, 'Connected!');
  socket.emit('connection', socket.id);
  
  socket.on('send-message', message => {
    io.emit('message', message);
  })

  socket.on("disconnect", () => {
    console.log(socket.id, 'Disconnected!');
  });
});

server.listen(port, () => console.log(`Listening on port ${port}`));