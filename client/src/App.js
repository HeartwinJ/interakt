import { Component } from 'react';
import MainLayout from './layouts/mainLayout';

class App extends Component {

  render() {
    return (
      <MainLayout />
    );
  }
}

export default App;
