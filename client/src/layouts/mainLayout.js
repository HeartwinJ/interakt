import { Component } from "react";
import { Row, Col } from "react-bootstrap";
import socketIOClient from "socket.io-client";
import ChannelList from "../widgets/ChannelList";
import ChatBox from "../widgets/ChatBox";
import MessageBar from "../widgets/MessageBar";

import Message from '../models/message'

export default class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { channels: [], messages: []};

    this.socket = socketIOClient('http://127.0.0.1:3001');

    this.sendMessage = this.sendMessage.bind(this);
  }

  sendMessage(messageData) {
    let message = {
      sender: this.socket.id,
      channel: '',
      data: messageData
    };
    this.socket.emit('send-message', message);
  }

  addMessage(message) {
    this.setState(prevState => ({
      messages: [...prevState.messages, new Message(message.sender, message.channel, message.data)]
    }));
  }

  componentDidMount() {
    this.socket.on("connection", data => {
      console.log('Connected to server with ID:', data);
      this.setState({});
    });
    this.socket.on('message', message => {
      this.addMessage(message);
    });
  }

  componentWillUnmount() {
    this.socket.disconnect();
  }

  render() {
    return (
      <div>
        <Row className="h-100 m-0">
          <Col xs={3} className="p-0 border-right border-secondary">
            <ChannelList channels={ this.state.channels } />
          </Col>
          <Col className="p-0 d-flex flex-column">
            <div className="flex-grow-1">
              <ChatBox messages={this.state.messages} myId={ this.socket.id }/>
            </div>
            <MessageBar onSend={ this.sendMessage }/>
          </Col>
        </Row>
      </div>
    );
  }
}