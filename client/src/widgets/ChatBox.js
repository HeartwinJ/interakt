import { Component } from "react";
import ChatBubble from './ChatBubble';

export default class ChatBox extends Component {
  render() {
    const { messages, myId } = this.props;
    return (
      <div className="h-100 p-3 d-flex flex-column justify-content-end">
        {
          messages.map((msg, index) => {
            return (
              <ChatBubble
                key={index}
                message={msg.data}
                sender={msg.sender === myId ? 'me' : ''}>
              </ChatBubble>
            );
          })
        }
      </div>
    );
  }
}