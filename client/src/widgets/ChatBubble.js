import { Component } from "react";

import '../styles/chatBubble.css';

export default class ChatBubble extends Component {
  render() {
    if (this.props.sender === 'me') {
      return (
        <div className="text-right text-white">
          <div className="bg-primary p-2 d-inline-block chat-bubble-me">{ this.props.message }</div>
        </div>
      );
    }
    else {
      return (
        <div className="text-left text-white">
          <div className="bg-success p-2 d-inline-block chat-bubble">{ this.props.message }</div>
        </div>
      );
    }
  }
}