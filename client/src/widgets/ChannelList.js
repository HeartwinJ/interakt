import { Component } from "react";
import Channel from './Channel';

export default class ChannelList extends Component {
  render() {
    return (
      <div>
        <Channel channelName="Channel 1"/>
        <Channel channelName="Channel 2"/>
        <Channel channelName="Channel 3"/>
        <Channel channelName="Channel 4"/>
        <Channel channelName="Channel 5"/>
      </div>
    );
  }
}