import { Component } from "react";
import { InputGroup, FormControl, Button } from "react-bootstrap";

export default class MessageBar extends Component {
  constructor(props) {
    super(props);

    this.state = { msg: '' };

    this.handleSend = this.handleSend.bind(this);
  }

  handleSend() {
    this.props.onSend(this.state.msg);
    this.setState({ msg: '' });
  }

  render() {
    return (
      <div className="p-3 border-top border-secondary">
        <InputGroup>
          <FormControl
            placeholder="Enter Message"
            value={this.state.msg}
            onChange={event => {
              this.setState({ msg: event.target.value });
            }}
            onKeyPress={event => {
              if (event.key === "Enter") {
                this.handleSend();
              }
            }}
          />
          <InputGroup.Append>
            <Button variant="outline-success" onClick={this.handleSend}>Send</Button>
          </InputGroup.Append>
        </InputGroup>
      </div>
    );
  }
}