import { Component } from "react";
import { Badge } from "react-bootstrap";

export default class Channel extends Component {
  render() {
    return (
      <div className="p-2 border-bottom border-secondary d-flex justify-content-between">
        <span>{ this.props.channelName }</span>
        <Badge>0</Badge>
      </div>
    );
  }
}