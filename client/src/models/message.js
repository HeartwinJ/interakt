export default function Message(sender, channel, data) {
  this.sender = sender;
  this.channel = channel;
  this.data = data;
}